//
//  ViewModel.swift
//  Cine Sky
//
//  Created by Celso Andre on 13/05/19.
//  Copyright © 2019 Celso Andre. All rights reserved.
//

import Foundation


class ViewModel {
    


    var network: NetworkManager?
    var movies: [Movie]?

    init() {
        network = NetworkManager()
    }

    func getMovies(completation: @escaping (Bool) -> ()){

        guard let network = network else { return }

        network.getMovies { (movies) in
            self.movies = movies
            completation(true)
        }


    }

    func getMovie(_ indexPath: IndexPath) -> Movie? {
        return movies?[indexPath.row]
    }

    func countMovies() -> Int {

        if let movies = movies, movies.count > 0 {
            return movies.count
        }

        return 0
    }






}
