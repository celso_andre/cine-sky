//
//  NetworkManager.swift
//  Cine Sky
//
//  Created by Celso Andre on 13/05/19.
//  Copyright © 2019 Celso Andre. All rights reserved.
//

import Foundation
import Alamofire

//protocol NetworkManagerDelegate {
//    func dataReceived(data: Any?, error: NSError?)
//}

class NetworkManager: NSObject {
    var baseURL: String
    //var delegate: NetworkManagerDelegate?

    override init() {
        self.baseURL = Constants.baseURLString

        super.init()
    }

    func getMovies(completion: @escaping ( _ movies: [Movie]) -> ()) {

        Alamofire.request(baseURL).responseJSON { (response) in
            switch response.result {
            case .success:
                guard let data = response.data else {
                    return
                }

                let movies = try? JSONDecoder().decode([Movie].self, from: data)

                if let movies = movies {
                    completion(movies)
                }



            case .failure(let error):
                NSLog(error.localizedDescription)
            }
        }
    }






}
