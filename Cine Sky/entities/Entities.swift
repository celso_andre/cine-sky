//
//  Entities.swift
//  Cine Sky
//
//  Created by Celso Andre on 13/05/19.
//  Copyright © 2019 Celso Andre. All rights reserved.
//

import Foundation

struct Movie : Codable {
    let title : String?
    let overview : String?
    let duration : String?
    let release_year : String?
    let cover_url : String?
    let backdrops_url : [String]?
    let id : String?

    enum CodingKeys: String, CodingKey {

        case title = "title"
        case overview = "overview"
        case duration = "duration"
        case release_year = "release_year"
        case cover_url = "cover_url"
        case backdrops_url = "backdrops_url"
        case id = "id"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        overview = try values.decodeIfPresent(String.self, forKey: .overview)
        duration = try values.decodeIfPresent(String.self, forKey: .duration)
        release_year = try values.decodeIfPresent(String.self, forKey: .release_year)
        cover_url = try values.decodeIfPresent(String.self, forKey: .cover_url)
        backdrops_url = try values.decodeIfPresent([String].self, forKey: .backdrops_url)
        id = try values.decodeIfPresent(String.self, forKey: .id)
    }
}
