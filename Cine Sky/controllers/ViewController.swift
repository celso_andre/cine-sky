//
//  ViewController.swift
//  Cine Sky
//
//  Created by Celso Andre on 13/05/19.
//  Copyright © 2019 Celso Andre. All rights reserved.
//

import UIKit
import Kingfisher
import SwiftOverlays

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    //Outlets
    @IBOutlet weak var collectionView: UICollectionView!

    //MARK:_Properties
    var viewModel: ViewModel
    //var navegation: NavegationViewController?

    public required init?(coder aDecoder: NSCoder) {

        self.viewModel = ViewModel()

        super.init(coder: aDecoder)
    }


    override func viewDidLoad() {
        super.viewDidLoad()

        guard let navBar = self.navigationController?.navigationBar else {
            return
        }

        setupNav(navBar)

        collectionView.delegate = self
        collectionView.dataSource = self
        self.showBlockingWaitOverlayWithText(message: "Carregando informações...")
        configureLayout()
        fetchMovies()
    }

    func setupNav(_ navBar: UINavigationBar) {
        let font = UIFont.boldSystemFont(ofSize: 25)
        navBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: font]
        navBar.barTintColor = UIColor.black
    }



    func configureLayout() {
        let layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        layout.minimumInteritemSpacing = 5
        layout.minimumLineSpacing = 5
        let size = CGSize(width: (view.frame.width / 2) - 20, height: view.frame.height / 3)
        layout.itemSize = size
    }

    func fetchMovies(){
        viewModel.getMovies { result in
            print(result)
            if result {
                self.collectionView.reloadData()
            }
            SwiftOverlays.removeAllBlockingOverlays()
        }

    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.countMovies()
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! MovieCollectionViewCell

        if let movie =  viewModel.getMovie(indexPath) {

            cell.movieLabel.text = movie.title

            if let urlImage = movie.cover_url {

                let url = URL(string: urlImage)
                

//


                let processor = DownsamplingImageProcessor(size: cell.movieImageView.frame.size)
                    >> RoundCornerImageProcessor(cornerRadius: 0)
                cell.movieImageView.kf.indicatorType = .activity
                cell.movieImageView.kf.setImage(
                    with: url,
                    placeholder: UIImage(named: "placeholderImage"),
                    options: [
                        .processor(processor),
                        .scaleFactor(UIScreen.main.scale),
                        .transition(.fade(1)),
                        .cacheOriginalImage
                    ])
                {
                    result in
                    switch result {
                    case .success(let value):
                        print("Task done for: \(value.source.url?.absoluteString ?? "")")
                    case .failure(let error):
                        print("Job failed: \(error.localizedDescription)")
                        cell.movieImageView.image = UIImage(named: "errorImage")
                    }
                }

            }


        }

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let movie = viewModel.getMovie(indexPath)
        print(movie?.title)

        self.performSegue(withIdentifier: Constants.segueforDetails, sender: self)

    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier == Constants.segueforDetails {
            let detailsVC = segue.destination as! DetailsViewController
            let indexPath = collectionView.indexPathsForSelectedItems

            if let indexPath = indexPath, let lastIndexPath = indexPath.last {
                detailsVC.movie = viewModel.getMovie(lastIndexPath)   //[lastRowSelected]
            }

        }


    }

    

    
}

