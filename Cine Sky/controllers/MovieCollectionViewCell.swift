//
//  MovieCollectionViewCell.swift
//  Cine Sky
//
//  Created by Celso Andre on 13/05/19.
//  Copyright © 2019 Celso Andre. All rights reserved.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var movieImageView: UIImageView!
    
    @IBOutlet weak var movieLabel: UILabel!
    

}
