//
//  DetailsViewController.swift
//  Cine Sky
//
//  Created by Celso Andre on 13/05/19.
//  Copyright © 2019 Celso Andre. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    //MARK:- Outlet

    @IBOutlet weak var backDropsImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var releaseYearLabel: UILabel!

    @IBOutlet weak var overViewText: UITextView!
    //MARK:- Properties
    var movie: Movie?

    override func viewDidLoad() {
        super.viewDidLoad()

        guard let navBar = self.navigationController?.navigationBar else {
            return
        }

        setupNav(navBar)

        if let movie = movie {
            self.title = movie.title
            titleLabel.text = movie.title
            overViewText.text = movie.overview
            releaseYearLabel.text = movie.release_year


            if let backdrops = movie.backdrops_url, let url = URL(string: backdrops[0]){
                backDropsImageView.kf.setImage(with: url)
            }


        }
    }



    func setupNav(_ navBar: UINavigationBar) {
        let font = UIFont.boldSystemFont(ofSize: 18)
        navBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: font]
        navBar.barTintColor = UIColor.black
    }




    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
