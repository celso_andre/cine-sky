//
//  Constants.swift
//  Cine Sky
//
//  Created by Celso Andre on 13/05/19.
//  Copyright © 2019 Celso Andre. All rights reserved.
//

import Foundation
class Constants {
    //MARKS:- URLs
    static let baseURLString: String = "https://sky-exercise.herokuapp.com/api/Movies"

     //MARKS:- Segues
    static let segueforDetails: String = "segueForDetails"
}
