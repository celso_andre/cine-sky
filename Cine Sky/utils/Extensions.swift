//
//  Extensions.swift
//  Cine Sky
//
//  Created by Celso Andre on 13/05/19.
//  Copyright © 2019 Celso Andre. All rights reserved.
//

import Foundation
import UIKit
import SwiftOverlays

extension UIViewController{
    func showBlockingWaitOverlayWithText(message: String) {
         SwiftOverlays.showBlockingWaitOverlayWithText(message)
    }


    func removeAllBlockingOverlays(){
        SwiftOverlays.removeAllBlockingOverlays()
    }
}


